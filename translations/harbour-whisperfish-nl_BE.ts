<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl_BE">
<context>
    <name></name>
    <message id="whisperfish-cover-new-label">
        <location filename="../qml/cover/UnreadLabel.qml" line="25"/>
        <source>New</source>
        <extracomment>Cover new message label</extracomment>
        <translation>Nieuw</translation>
    </message>
    <message id="whisperfish-session-has-attachment">
        <location filename="../qml/delegates/Session.qml" line="69"/>
        <source>Attachment</source>
        <extracomment>Session contains an attachment label</extracomment>
        <translation>Bijlage</translation>
    </message>
    <message id="whisperfish-session-delete-all">
        <location filename="../qml/delegates/Session.qml" line="117"/>
        <source>Deleting all messages</source>
        <extracomment>Delete all messages from session</extracomment>
        <translation>Alle berichten worden verwijderd</translation>
    </message>
    <message id="whisperfish-delete-session">
        <location filename="../qml/delegates/Session.qml" line="132"/>
        <source>Delete Conversation</source>
        <extracomment>Delete all messages from session menu</extracomment>
        <translation>Gesprek verwijderen</translation>
    </message>
    <message id="whisperfish-notification-default-message">
        <location filename="../qml/harbour-whisperfish.qml" line="41"/>
        <source>New Message</source>
        <extracomment>Default label for new message notification</extracomment>
        <translation>Nieuw bericht</translation>
    </message>
    <message id="whisperfish-session-section-today">
        <location filename="../qml/pages/Main.qml" line="117"/>
        <source>Today</source>
        <extracomment>Session section label for today</extracomment>
        <translation>Vandaag</translation>
    </message>
    <message id="whisperfish-session-section-yesterday">
        <location filename="../qml/pages/Main.qml" line="122"/>
        <source>Yesterday</source>
        <extracomment>Session section label for yesterday</extracomment>
        <translation>Gisteren</translation>
    </message>
    <message id="whisperfish-session-section-older">
        <location filename="../qml/pages/Main.qml" line="127"/>
        <source>Older</source>
        <extracomment>Session section label for older</extracomment>
        <translation>Ouder</translation>
    </message>
    <message id="whisperfish-about">
        <location filename="../qml/pages/About.qml" line="20"/>
        <source>About Whisperfish</source>
        <extracomment>Title for about page</extracomment>
        <translation>Over Whisperfish</translation>
    </message>
    <message id="whisperfish-version">
        <location filename="../qml/pages/About.qml" line="33"/>
        <source>Whisperfish v%1</source>
        <extracomment>Whisperfish version string</extracomment>
        <translation>Whisperfish v%1</translation>
    </message>
    <message id="whisperfish-description">
        <location filename="../qml/pages/About.qml" line="43"/>
        <source>Signal client for Sailfish OS</source>
        <extracomment>Whisperfish description</extracomment>
        <translation>Signal-cliënt voor Sailfish OS</translation>
    </message>
    <message id="whisperfish-build-id">
        <location filename="../qml/pages/About.qml" line="54"/>
        <source>Build ID: %1</source>
        <extracomment>Whisperfish long version string and build ID</extracomment>
        <translation>Serienummer: %1</translation>
    </message>
    <message id="whisperfish-copyright">
        <location filename="../qml/pages/About.qml" line="64"/>
        <source>Copyright</source>
        <extracomment>Copyright</extracomment>
        <translation>Auteursrecht</translation>
    </message>
    <message id="whisperfish-liberapay">
        <location filename="../qml/pages/About.qml" line="79"/>
        <source>Support on Liberapay</source>
        <extracomment>Support on Liberapay</extracomment>
        <translation>Steun mij op Liberapay</translation>
    </message>
    <message id="whisperfish-source-code">
        <location filename="../qml/pages/About.qml" line="89"/>
        <source>Source Code</source>
        <extracomment>Source Code</extracomment>
        <translation>Broncode</translation>
    </message>
    <message id="whisperfish-bug-report">
        <location filename="../qml/pages/About.qml" line="99"/>
        <source>Report a Bug</source>
        <extracomment>Report a Bug</extracomment>
        <translation>Meld een probleem</translation>
    </message>
    <message id="whisperfish-extra-copyright">
        <location filename="../qml/pages/About.qml" line="108"/>
        <source>Additional Copyright</source>
        <extracomment>Additional Copyright</extracomment>
        <translation>Bijkomend auteursrecht</translation>
    </message>
    <message id="whisperfish-add-confirm">
        <location filename="../qml/pages/AddDevice.qml" line="25"/>
        <source>Add</source>
        <extracomment>&quot;Add&quot; message, shown in the link device dialog</extracomment>
        <translation>Voeg toe</translation>
    </message>
    <message id="whisperfish-add-device">
        <location filename="../qml/pages/AddDevice.qml" line="33"/>
        <source>Add Device</source>
        <extracomment>Add Device, shown as pull-down menu item</extracomment>
        <translation>Apparaat toevoegen</translation>
    </message>
    <message id="whisperfish-device-url">
        <location filename="../qml/pages/AddDevice.qml" line="42"/>
        <source>Device URL</source>
        <extracomment>Device URL, text input for pasting the QR-scanned code</extracomment>
        <translation>Apparaat-URL</translation>
    </message>
    <message id="whisperfish-device-link-instructions">
        <location filename="../qml/pages/AddDevice.qml" line="56"/>
        <source>Install Signal Desktop. Use the CodeReader application to scan the QR code displayed on Signal Desktop and copy and paste the URL here.</source>
        <extracomment>Instructions on how to scan QR code for device linking</extracomment>
        <translation>Installeerd Signal Desktop. Gebruikt den app ‘CodeReader’ voor de QR-code te scannen die word weergegeven op Signal Desktop, en kopieerd en plakt den URL hier.</translation>
    </message>
    <message id="whisperfish-attachment-from-self">
        <location filename="../qml/pages/AttachmentPage.qml" line="25"/>
        <location filename="../qml/pages/VideoAttachment.qml" line="24"/>
        <source>Me</source>
        <extracomment>Personalized placeholder showing the attachment is from oneself</extracomment>
        <translation>Ik</translation>
    </message>
    <message id="whisperfish-attachment-from-contact">
        <location filename="../qml/pages/AttachmentPage.qml" line="28"/>
        <location filename="../qml/pages/VideoAttachment.qml" line="27"/>
        <source>From %1</source>
        <extracomment>Personalized placeholder showing the attachment is from contact</extracomment>
        <translation>Van %1</translation>
    </message>
    <message id="whisperfish-chatinput-contact">
        <location filename="../qml/pages/WFChatTextInput.qml" line="112"/>
        <source>Hi %1</source>
        <extracomment>Personalized placeholder for chat input, e.g. &quot;Hi John&quot;</extracomment>
        <translation>Hé %1</translation>
    </message>
    <message id="whisperfish-chatinput-generic">
        <location filename="../qml/pages/WFChatTextInput.qml" line="115"/>
        <source>Hi</source>
        <extracomment>Generic placeholder for chat input</extracomment>
        <translation>Hé</translation>
    </message>
    <message id="whisperfish-select-file">
        <location filename="../qml/pages/WFChatTextInput.qml" line="197"/>
        <source>Select file</source>
        <extracomment>Title for file picker page</extracomment>
        <translation>Selecteerd een bestand</translation>
    </message>
    <message id="whisperfish-choose-country-code">
        <location filename="../qml/pages/CountryCodeDialog.qml" line="17"/>
        <source>Choose Country Code</source>
        <extracomment>Directions for choosing country code</extracomment>
        <translation>Selecteerd een landcode</translation>
    </message>
    <message id="whisperfish-select-picture">
        <location filename="../qml/pages/ImagePicker.qml" line="44"/>
        <source>Select picture</source>
        <extracomment>Title for image picker page</extracomment>
        <translation>Selecteerd een afbeelding</translation>
    </message>
    <message id="whisperfish-add-linked-device">
        <location filename="../qml/pages/LinkedDevices.qml" line="17"/>
        <source>Add</source>
        <extracomment>Menu option to add new linked device</extracomment>
        <translation>Toevoegen</translation>
    </message>
    <message id="whisperfish-refresh-linked-devices">
        <location filename="../qml/pages/LinkedDevices.qml" line="30"/>
        <source>Refresh</source>
        <extracomment>Menu option to refresh linked devices</extracomment>
        <translation>Vernieuwen</translation>
    </message>
    <message id="whisperfish-linked-devices">
        <location filename="../qml/pages/LinkedDevices.qml" line="39"/>
        <source>Linked Devices</source>
        <extracomment>Title for Linked Devices page</extracomment>
        <translation>Gekoppelde apparaten</translation>
    </message>
    <message id="whisperfish-device-unlink-message">
        <location filename="../qml/pages/LinkedDevices.qml" line="49"/>
        <source>Unlinking</source>
        <extracomment>Unlinking remorse info message</extracomment>
        <translation>Apparaat word ontkoppeld</translation>
    </message>
    <message id="whisperfish-device-name">
        <location filename="../qml/pages/LinkedDevices.qml" line="64"/>
        <source>Device %1</source>
        <extracomment>Linked device name</extracomment>
        <translation>Apparaat %1</translation>
    </message>
    <message id="whisperfish-device-link-date">
        <location filename="../qml/pages/LinkedDevices.qml" line="76"/>
        <source>Linked: %1</source>
        <extracomment>Linked device date</extracomment>
        <translation>Gekoppeld op %1</translation>
    </message>
    <message id="whisperfish-device-last-active">
        <location filename="../qml/pages/LinkedDevices.qml" line="94"/>
        <source>Last active: %1</source>
        <extracomment>Linked device last active date</extracomment>
        <translation>Laatst actief op %1</translation>
    </message>
    <message id="whisperfish-device-unlink">
        <location filename="../qml/pages/LinkedDevices.qml" line="114"/>
        <source>Unlink</source>
        <extracomment>Device unlink menu option</extracomment>
        <translation>Ontkoppelen</translation>
    </message>
    <message id="whisperfish-registration-complete">
        <location filename="../qml/pages/Main.qml" line="27"/>
        <source>Registration complete!</source>
        <extracomment>Registration complete remorse message</extracomment>
        <translation>Registratie voltooid!</translation>
    </message>
    <message id="whisperfish-error-invalid-datastore">
        <location filename="../qml/pages/Main.qml" line="32"/>
        <source>ERROR - Failed to setup datastore</source>
        <extracomment>Failed to setup datastore error message</extracomment>
        <translation>FOUT - instellen van gegevensopslag mislukt</translation>
    </message>
    <message id="whisperfish-error-invalid-number">
        <location filename="../qml/pages/Main.qml" line="37"/>
        <source>ERROR - Invalid phone number registered with Signal</source>
        <extracomment>Invalid phone number error message</extracomment>
        <translation>FOUT - ongeldig gsmnummer geregistreerd bij Signal</translation>
    </message>
    <message id="whisperfish-error-setup-client">
        <location filename="../qml/pages/Main.qml" line="42"/>
        <source>ERROR - Failed to setup Signal client</source>
        <extracomment>Failed to setup signal client error message</extracomment>
        <translation>FOUT- instellen van Signal-cliënt mislukt</translation>
    </message>
    <message id="whisperfish-about-menu">
        <location filename="../qml/pages/Main.qml" line="58"/>
        <source>About Whisperfish</source>
        <extracomment>About whisperfish menu item</extracomment>
        <translation>Over Whisperfish</translation>
    </message>
    <message id="whisperfish-settings-menu">
        <location filename="../qml/pages/Main.qml" line="64"/>
        <source>Settings</source>
        <extracomment>Whisperfish settings menu item</extracomment>
        <translation>Instellingen</translation>
    </message>
    <message id="whisperfish-new-group-menu">
        <location filename="../qml/pages/Main.qml" line="71"/>
        <source>New Group</source>
        <extracomment>Whisperfish new group menu item</extracomment>
        <translation>Nieuwe groep</translation>
    </message>
    <message id="whisperfish-new-message-menu">
        <location filename="../qml/pages/Main.qml" line="78"/>
        <source>New Message</source>
        <extracomment>Whisperfish new message menu item</extracomment>
        <translation>Nieuw bericht</translation>
    </message>
    <message id="whisperfish-no-messages-found">
        <location filename="../qml/pages/Main.qml" line="92"/>
        <source>No messages</source>
        <extracomment>Whisperfish no messages found message</extracomment>
        <translation>Geen berichten</translation>
    </message>
    <message id="whisperfish-registration-required-message">
        <location filename="../qml/pages/Main.qml" line="97"/>
        <source>Registration required</source>
        <extracomment>Whisperfish registration required message</extracomment>
        <translation>Registratie vereist</translation>
    </message>
    <message id="whisperfish-locked-message">
        <location filename="../qml/pages/Main.qml" line="101"/>
        <source>Locked</source>
        <extracomment>Whisperfish locked message</extracomment>
        <translation>Vergrendeld</translation>
    </message>
    <message id="whisperfish-group-label">
        <location filename="../qml/pages/MessagesView.qml" line="89"/>
        <source>Group: %1</source>
        <extracomment>Group message label</extracomment>
        <translation>Groep: %1</translation>
    </message>
    <message id="whisperfish-delete-message">
        <location filename="../qml/pages/MessagesView.qml" line="102"/>
        <source>Deleteing</source>
        <oldsource>Deleting</oldsource>
        <extracomment>Deleteing message remorse</extracomment>
        <translation>Word verwijderd</translation>
    </message>
    <message id="whisperfish-resend-message">
        <location filename="../qml/pages/MessagesView.qml" line="112"/>
        <source>Resending</source>
        <extracomment>Resend message remorse</extracomment>
        <translation>Word opnieuw verzonden</translation>
    </message>
    <message id="whisperfish-copy-message-menu">
        <location filename="../qml/pages/MessagesView.qml" line="135"/>
        <source>Copy</source>
        <extracomment>Copy message menu item</extracomment>
        <translation>Kopiëren</translation>
    </message>
    <message id="whisperfish-open-message-menu">
        <location filename="../qml/pages/MessagesView.qml" line="141"/>
        <source>Open</source>
        <extracomment>Open attachment message menu item</extracomment>
        <translation>Openen</translation>
    </message>
    <message id="whisperfish-delete-message-menu">
        <location filename="../qml/pages/MessagesView.qml" line="148"/>
        <source>Delete</source>
        <extracomment>Delete message menu item</extracomment>
        <translation>Verwijderen</translation>
    </message>
    <message id="whisperfish-resend-message-menu">
        <location filename="../qml/pages/MessagesView.qml" line="154"/>
        <source>Resend</source>
        <extracomment>Resend message menu item</extracomment>
        <translation>Opnieuw verzenden</translation>
    </message>
    <message id="whisperfish-reset-session-menu">
        <location filename="../qml/pages/VerifyIdentity.qml" line="18"/>
        <source>Reset Secure Session</source>
        <extracomment>Reset secure session menu item</extracomment>
        <translation>Beveiligde sessie opnieuw instellen</translation>
    </message>
    <message id="whisperfish-reset-session-message">
        <location filename="../qml/pages/VerifyIdentity.qml" line="23"/>
        <source>Resetting secure session</source>
        <extracomment>Reset secure session remorse message</extracomment>
        <translation>Beveiligde sessie word opnieuw ingesteld</translation>
    </message>
    <message id="whisperfish-numeric-fingerprint-directions">
        <location filename="../qml/pages/VerifyIdentity.qml" line="63"/>
        <source>If you wish to verify the security of your end-to-end encryption with %1, compare the numbers above with the numbers on their device.</source>
        <extracomment>Numeric fingerprint instructions</extracomment>
        <translation>Vergelijkt de nummers hierboven met de nummers op het apparaat van %1 voor de veiligheid van ulder eind-tot-eind-versleuteling te verifiëren.</translation>
    </message>
    <message id="whisperfish-new-message-title">
        <location filename="../qml/pages/NewMessage.qml" line="64"/>
        <source>New message</source>
        <extracomment>New message page title</extracomment>
        <translation>Nieuw bericht</translation>
    </message>
    <message id="whisperfish-group-name-label">
        <location filename="../qml/pages/NewGroup.qml" line="46"/>
        <source>Group Name</source>
        <extracomment>Group name label</extracomment>
        <translation>Naam van groep</translation>
    </message>
    <message id="whisperfish-group-name-placeholder">
        <location filename="../qml/pages/NewGroup.qml" line="49"/>
        <source>Group Name</source>
        <extracomment>Group name placeholder</extracomment>
        <translation>Naam van groep</translation>
    </message>
    <message id="whisperfish-new-group-title">
        <location filename="../qml/pages/NewGroup.qml" line="37"/>
        <source>New Group</source>
        <extracomment>New group page title</extracomment>
        <translation>Nieuwe groep</translation>
    </message>
    <message id="whisperfish-new-message-recipient">
        <location filename="../qml/pages/NewMessage.qml" line="82"/>
        <location filename="../qml/pages/NewMessage.qml" line="86"/>
        <source>Recipient</source>
        <extracomment>New message recipient label
----------
Summary of all selected recipients, e.g. &quot;Bob, Jane, 75553243&quot;</extracomment>
        <translation>Ontvanger</translation>
    </message>
    <message id="whisperfish-new-group-message-members">
        <location filename="../qml/pages/NewGroup.qml" line="68"/>
        <location filename="../qml/pages/NewGroup.qml" line="72"/>
        <source>Members</source>
        <extracomment>New group message members label
----------
Summary of all selected recipients, e.g. &quot;Bob, Jane, 75553243&quot;</extracomment>
        <translation>Leden</translation>
    </message>
    <message id="whisperfish-error-invalid-group-name">
        <location filename="../qml/pages/NewGroup.qml" line="104"/>
        <source>Please name the group</source>
        <extracomment>Invalid group name error</extracomment>
        <translation>Geefd de groep ne naam</translation>
    </message>
    <message id="whisperfish-error-invalid-group-members">
        <location filename="../qml/pages/NewGroup.qml" line="100"/>
        <source>Please select group members</source>
        <extracomment>Invalid recipient error</extracomment>
        <translation>Selecteerd groepsleden</translation>
    </message>
    <message id="whisperfish-error-invalid-recipient">
        <location filename="../qml/pages/NewMessage.qml" line="122"/>
        <source>Invalid recipient</source>
        <extracomment>Invalid recipient error</extracomment>
        <translation>Ongeldigen ontvanger</translation>
    </message>
    <message id="whisperfish-enter-password">
        <location filename="../qml/pages/Password.qml" line="45"/>
        <source>Enter your password</source>
        <extracomment>Enter password prompt</extracomment>
        <translation>Voerd uw paswoord in</translation>
    </message>
    <message id="whisperfish-set-password">
        <location filename="../qml/pages/Password.qml" line="48"/>
        <source>Set your password</source>
        <extracomment>Set password prompt</extracomment>
        <translation>Steld uw paswoord in</translation>
    </message>
    <message id="whisperfish-password-label">
        <location filename="../qml/pages/Password.qml" line="58"/>
        <source>Password</source>
        <extracomment>Password label</extracomment>
        <translation>Paswoord</translation>
    </message>
    <message id="whisperfish-password-placeholder">
        <location filename="../qml/pages/Password.qml" line="61"/>
        <source>Password</source>
        <extracomment>Password placeholder</extracomment>
        <translation>Paswoord</translation>
    </message>
    <message id="whisperfish-verify-password-label">
        <location filename="../qml/pages/Password.qml" line="83"/>
        <source>Verify Password</source>
        <extracomment>Verify Password label</extracomment>
        <translation>Bevestigd uw paswoord</translation>
    </message>
    <message id="whisperfish-verify-password-placeholder">
        <location filename="../qml/pages/Password.qml" line="86"/>
        <source>Verify Password</source>
        <extracomment>Verify Password label</extracomment>
        <translation>Bevestigd uw paswoord</translation>
    </message>
    <message id="whisperfish-password-info">
        <location filename="../qml/pages/Password.qml" line="104"/>
        <source>Whisperfish stores identity keys, session state, and local message data encrypted on disk. The password you set is not stored anywhere and you will not be able to restore your data if you lose your password. Note: Attachments are currently stored unencrypted. You can disable storing of attachments in the Settings page.</source>
        <extracomment>Whisperfish password informational message</extracomment>
        <translation>Whisperfish slaat identiteitssleutels, sessiestatus en lokale berichtgegevens versleuteld op de schijf op. Het paswoord dat ge instelt word nergens opgeslagen, dus als ge het verliest zult ge uw gegevens niet kunnen herstellen. Let op: bijlagen worden momenteel onversleuteld opgeslagen. Ge kunt in de Instellingen het opslaan van bijlagen uitschakelen.</translation>
    </message>
    <message id="whisperfish-register-accept">
        <location filename="../qml/pages/Register.qml" line="25"/>
        <source>Register</source>
        <extracomment>Register accept text</extracomment>
        <translation>Registreren</translation>
    </message>
    <message id="whisperfish-registration-message">
        <location filename="../qml/pages/Register.qml" line="35"/>
        <source>Enter the phone number you want to register with Signal.</source>
        <extracomment>Registration message</extracomment>
        <translation>Voert het telefoonnummer in dat ge wilt registreren bij Signal.</translation>
    </message>
    <message id="whisperfish-phone-number-input-label">
        <location filename="../qml/pages/Register.qml" line="45"/>
        <source>International phone number</source>
        <extracomment>Phone number input</extracomment>
        <translation>Internationaal gsm-nummer</translation>
    </message>
    <message id="whisperfish-phone-number-input-placeholder">
        <location filename="../qml/pages/Register.qml" line="48"/>
        <source>+18875550100</source>
        <extracomment>Phone number placeholder</extracomment>
        <translation>+32474123456</translation>
    </message>
    <message id="whisperfish-share-contacts-label">
        <location filename="../qml/pages/Register.qml" line="60"/>
        <location filename="../qml/pages/Settings.qml" line="188"/>
        <source>Share Contacts</source>
        <extracomment>Share contacts label
----------
Settings page share contacts</extracomment>
        <translation>Contacten delen</translation>
    </message>
    <message id="whisperfish-share-contacts-description">
        <location filename="../qml/pages/Register.qml" line="63"/>
        <location filename="../qml/pages/Settings.qml" line="191"/>
        <source>Allow Signal to use your local contact list, to find other Signal users.</source>
        <extracomment>Share contacts description</extracomment>
        <translation>Geef Signal uw telefoonboek, om te checken welke maten ook Signal gebruiken.</translation>
    </message>
    <message id="whisperfish-verification-method-label">
        <location filename="../qml/pages/Register.qml" line="76"/>
        <source>Verification method</source>
        <extracomment>Verification method</extracomment>
        <translation>Verificatiemethode</translation>
    </message>
    <message id="whisperfish-use-voice-verification">
        <location filename="../qml/pages/Register.qml" line="87"/>
        <source>Use voice verification</source>
        <extracomment>Voice verification</extracomment>
        <translation>Oproepverificatie</translation>
    </message>
    <message id="whisperfish-use-text-verification">
        <location filename="../qml/pages/Register.qml" line="82"/>
        <source>Use text verification</source>
        <extracomment>Text verification</extracomment>
        <translation>SMS-verificatie</translation>
    </message>
    <message id="whisperfish-voice-registration-directions">
        <location filename="../qml/pages/Register.qml" line="104"/>
        <source>Signal will call you with a 6-digit verification code. Please be ready to write this down.</source>
        <extracomment>Registration directions</extracomment>
        <translation>Signal zal u bellen met ne verificatiecode van 6 tekens. Zorg dat ge deze ergens kund opschrijven.</translation>
    </message>
    <message id="whisperfish-text-registration-directions">
        <location filename="../qml/pages/Register.qml" line="115"/>
        <source>Signal will text you a 6-digit verification code.</source>
        <extracomment>Registration directions</extracomment>
        <translation>Signal stuurt u een SMS met een code van zes cijfers. Ge hebt die nodig in de volgende stap.</translation>
    </message>
    <message id="whisperfish-reset-peer-accept">
        <location filename="../qml/pages/PeerIdentityChanged.qml" line="24"/>
        <location filename="../qml/pages/ResetPeerIdentity.qml" line="24"/>
        <source>Confirm</source>
        <extracomment>Reset peer identity accept text</extracomment>
        <translation>Bevestigen</translation>
    </message>
    <message id="whisperfish-peer-not-trusted">
        <location filename="../qml/pages/PeerIdentityChanged.qml" line="32"/>
        <location filename="../qml/pages/ResetPeerIdentity.qml" line="32"/>
        <source>Peer identity is not trusted</source>
        <extracomment>Peer identity not trusted</extracomment>
        <translation>Peeridentiteit is niet vertrouwd</translation>
    </message>
    <message id="whisperfish-peer-not-trusted-message">
        <location filename="../qml/pages/PeerIdentityChanged.qml" line="42"/>
        <location filename="../qml/pages/ResetPeerIdentity.qml" line="42"/>
        <source>WARNING: %1 identity is no longer trusted. Tap Confirm to reset peer identity.</source>
        <extracomment>Peer identity not trusted message</extracomment>
        <translation>WAARSCHUWING: de identiteit van %1 is niet meer vertrouwd. Tikt op Bevestigen voor de peeridentiteit opnieuw in te stellen.</translation>
    </message>
    <message id="whisperfish-settings-linked-devices-menu">
        <location filename="../qml/pages/Settings.qml" line="25"/>
        <source>Linked Devices</source>
        <extracomment>Linked devices menu option</extracomment>
        <translation>Gekoppelde apparaten</translation>
    </message>
    <message id="whisperfish-settings-reconnect-menu">
        <location filename="../qml/pages/Settings.qml" line="31"/>
        <source>Reconnect</source>
        <extracomment>Reconnect menu</extracomment>
        <translation>Opnieuw verbinden</translation>
    </message>
    <message id="whisperfish-settings-refresh-contacts-menu">
        <location filename="../qml/pages/Settings.qml" line="39"/>
        <source>Refresh Contacts</source>
        <extracomment>Refresh contacts menu</extracomment>
        <translation>Contacten vernieuwen</translation>
    </message>
    <message id="whisperfish-settings-title">
        <location filename="../qml/pages/Settings.qml" line="56"/>
        <source>Whisperfish Settings</source>
        <extracomment>Settings page title</extracomment>
        <translation>Instellingen</translation>
    </message>
    <message id="whisperfish-settings-identity-section-label">
        <location filename="../qml/pages/Settings.qml" line="63"/>
        <source>My Identity</source>
        <extracomment>Settings page My identity section label</extracomment>
        <translation>Mijn identiteit</translation>
    </message>
    <message id="whisperfish-settings-my-phone-number">
        <location filename="../qml/pages/Settings.qml" line="72"/>
        <source>My Phone</source>
        <extracomment>Settings page My phone number</extracomment>
        <translation>Mijne geregisteerde gsm-nummer</translation>
    </message>
    <message id="whisperfish-settings-my-uuid">
        <location filename="../qml/pages/Settings.qml" line="82"/>
        <source>My UUID registration number</source>
        <extracomment>Settings page My UUID</extracomment>
        <translation>Mijnen UUID</translation>
    </message>
    <message id="whisperfish-settings-identity-label">
        <location filename="../qml/pages/Settings.qml" line="93"/>
        <source>Identity</source>
        <extracomment>Settings page Identity label</extracomment>
        <translation>Gijzelf</translation>
    </message>
    <message id="whisperfish-settings-notifications-section">
        <location filename="../qml/pages/Settings.qml" line="102"/>
        <source>Notifications</source>
        <extracomment>Settings page notifications section</extracomment>
        <translation>Meldingen</translation>
    </message>
    <message id="whisperfish-settings-notifications-enable">
        <location filename="../qml/pages/Settings.qml" line="109"/>
        <source>Enable notifications</source>
        <extracomment>Settings page notifications enable</extracomment>
        <translation>Ballonnekes Ingeschakeld</translation>
    </message>
    <message id="whisperfish-settings-notifications-enable-description">
        <location filename="../qml/pages/Settings.qml" line="112"/>
        <source>If turned off, Whisperfish will not send any notification</source>
        <extracomment>Settings page notifications enable description</extracomment>
        <translation>Als ge dees afzet, krijgt ge geen ballonnekes en bellekes.</translation>
    </message>
    <message id="whisperfish-settings-notifications-show-body">
        <location filename="../qml/pages/Settings.qml" line="125"/>
        <source>Show Message Body</source>
        <extracomment>Settings page notifications show message body</extracomment>
        <translation>Berichtinhoud weergeven</translation>
    </message>
    <message id="whisperfish-settings-notifications-show-body-description">
        <location filename="../qml/pages/Settings.qml" line="128"/>
        <source>If turned off, Whisperfish will only show the sender of a message, not the contents.</source>
        <extracomment>Settings page notifications show message body description</extracomment>
        <translation>Als ge dees afzet, ziet ge in een ballonneke enkel wie er u stuurt, niet wat die u stuurt.</translation>
    </message>
    <message id="whisperfish-settings-general-section">
        <location filename="../qml/pages/Settings.qml" line="143"/>
        <source>General</source>
        <extracomment>Settings page general section</extracomment>
        <translation>Algemene dinges</translation>
    </message>
    <message id="whisperfish-settings-country-code">
        <location filename="../qml/pages/Settings.qml" line="150"/>
        <source>Country Code</source>
        <extracomment>Settings page country code</extracomment>
        <translation>Landcode</translation>
    </message>
    <message id="whisperfish-settings-country-code-description">
        <location filename="../qml/pages/Settings.qml" line="153"/>
        <source>The selected country code determines what happens when a local phone number is entered.</source>
        <extracomment>Settings page country code description</extracomment>
        <translation>Hier kiest ge België, want dan werken 04-nummers gelijk ge zou verwachten!</translation>
    </message>
    <message id="whisperfish-settings-save-attachments">
        <location filename="../qml/pages/Settings.qml" line="168"/>
        <source>Save Attachments</source>
        <extracomment>Settings page save attachments</extracomment>
        <translation>Bijlagen opslaan</translation>
    </message>
    <message id="whisperfish-settings-save-attachments-description">
        <location filename="../qml/pages/Settings.qml" line="172"/>
        <source>Attachments are stored at %1</source>
        <extracomment>Settings page save attachments description</extracomment>
        <translation>Bijlagen worden bijgehouden in %1.</translation>
    </message>
    <message id="whisperfish-settings-enable-enter-send">
        <location filename="../qml/pages/Settings.qml" line="205"/>
        <source>Return key send</source>
        <oldsource>EnterKey Send</oldsource>
        <extracomment>Settings page enable enter send</extracomment>
        <translation>Sturen met Enter</translation>
    </message>
    <message id="whisperfish-settings-enable-enter-send-description">
        <location filename="../qml/pages/Settings.qml" line="208"/>
        <source>When enabled, the return key functions as a send key. Otherwise, the return key can be used for multi-line messages.</source>
        <extracomment>Settings page enable enter send description</extracomment>
        <translation>Als ge dees opzet, worden berichten direct gestuurd met de enterknop. Anders kunt ge enter gebruiken voor lange berichten.</translation>
    </message>
    <message id="whisperfish-settings-advanced-section">
        <location filename="../qml/pages/Settings.qml" line="223"/>
        <source>Advanced</source>
        <extracomment>Settings page advanced section</extracomment>
        <translation>Moeilijkere dinges</translation>
    </message>
    <message id="whisperfish-settings-incognito-mode">
        <location filename="../qml/pages/Settings.qml" line="230"/>
        <source>Incognito Mode</source>
        <extracomment>Settings page incognito mode</extracomment>
        <translation>Incognitomodus</translation>
    </message>
    <message id="whisperfish-settings-incognito-mode-description">
        <location filename="../qml/pages/Settings.qml" line="233"/>
        <source>Incognito Mode disables storage entirely. No attachments nor messages are saved, messages are visible until restart.</source>
        <extracomment>Settings page incognito mode description</extracomment>
        <translation>Met Incognitomodus houdt ge geen berichten bij. Geen enkel. Ook geen bijlagen. Ge ziet berichten tot ge Whisperfish herstart.</translation>
    </message>
    <message id="whisperfish-settings-restarting-message">
        <location filename="../qml/pages/Settings.qml" line="241"/>
        <source>Restart Whisperfish...</source>
        <extracomment>Restart whisperfish message</extracomment>
        <translation>Herstart Whisperfish…</translation>
    </message>
    <message id="whisperfish-settings-scale-image-attachments">
        <location filename="../qml/pages/Settings.qml" line="254"/>
        <source>Scale JPEG Attachments</source>
        <extracomment>Settings page scale image attachments</extracomment>
        <translation>JPEG-bijlagen schalen</translation>
    </message>
    <message id="whisperfish-settings-scale-image-attachments-description">
        <location filename="../qml/pages/Settings.qml" line="257"/>
        <source>Scale down JPEG attachments to save on bandwidth.</source>
        <extracomment>Settings page scale image attachments description</extracomment>
        <translation>Hiermeer schaalt ge JPEG fotokes, om te besparen op duur Belgisch internet.</translation>
    </message>
    <message id="whisperfish-settings-debug-mode">
        <location filename="../qml/pages/Settings.qml" line="271"/>
        <source>Debug mode</source>
        <extracomment>Settings page: debug info toggle</extracomment>
        <translation>Insectenvangermodus</translation>
    </message>
    <message id="whisperfish-settings-debug-mode-description">
        <location filename="../qml/pages/Settings.qml" line="274"/>
        <source>Show debugging information in the user interface.</source>
        <extracomment>Settings page: debug info toggle extended description</extracomment>
        <translation>Als ge beestjes wilt vangen, kunt ge dees aanzetten om meer zakes te zien in den app.</translation>
    </message>
    <message id="whisperfish-settings-stats-section">
        <location filename="../qml/pages/Settings.qml" line="289"/>
        <source>Statistics</source>
        <extracomment>Settings page stats section</extracomment>
        <translation>Statistieken</translation>
    </message>
    <message id="whisperfish-settings-websocket">
        <location filename="../qml/pages/Settings.qml" line="294"/>
        <source>Websocket Status</source>
        <extracomment>Settings page websocket status</extracomment>
        <translation>Websocketstatus</translation>
    </message>
    <message id="whisperfish-settings-connected">
        <location filename="../qml/pages/Settings.qml" line="298"/>
        <source>Connected</source>
        <extracomment>Settings page connected message</extracomment>
        <translation>Verbonden</translation>
    </message>
    <message id="whisperfish-settings-disconnected">
        <location filename="../qml/pages/Settings.qml" line="301"/>
        <source>Disconnected</source>
        <extracomment>Settings page disconnected message</extracomment>
        <translation>Verbinding verbroken</translation>
    </message>
    <message id="whisperfish-settings-unsent-messages">
        <location filename="../qml/pages/Settings.qml" line="306"/>
        <source>Unsent Messages</source>
        <extracomment>Settings page unsent messages</extracomment>
        <translation>Niet-verzonden berichten</translation>
    </message>
    <message id="whisperfish-settings-total-sessions">
        <location filename="../qml/pages/Settings.qml" line="312"/>
        <source>Total Sessions</source>
        <extracomment>Settings page total sessions</extracomment>
        <translation>Totaal aantal sessies</translation>
    </message>
    <message id="whisperfish-settings-total-messages">
        <location filename="../qml/pages/Settings.qml" line="318"/>
        <source>Total Messages</source>
        <extracomment>Settings page total messages</extracomment>
        <translation>Totaal aantal berichten</translation>
    </message>
    <message id="whisperfish-settings-total-contacts">
        <location filename="../qml/pages/Settings.qml" line="324"/>
        <source>Signal Contacts</source>
        <extracomment>Settings page total signal contacts</extracomment>
        <translation>Contacten op Signal</translation>
    </message>
    <message id="whisperfish-settings-encrypted-keystore">
        <location filename="../qml/pages/Settings.qml" line="330"/>
        <source>Encrypted Key Store</source>
        <extracomment>Settings page encrypted key store</extracomment>
        <translation>Versleutelde sleutelopslag</translation>
    </message>
    <message id="whisperfish-settings-encrypted-keystore-enabled">
        <location filename="../qml/pages/Settings.qml" line="334"/>
        <source>Enabled</source>
        <extracomment>Settings page encrypted key store enabled</extracomment>
        <translation>Ingeschakeld</translation>
    </message>
    <message id="whisperfish-settings-encrypted-keystore-disabled">
        <location filename="../qml/pages/Settings.qml" line="337"/>
        <source>Disabled</source>
        <extracomment>Settings page encrypted key store disabled</extracomment>
        <translation>Uitgeschakeld</translation>
    </message>
    <message id="whisperfish-settings-encrypted-db">
        <location filename="../qml/pages/Settings.qml" line="342"/>
        <source>Encrypted Database</source>
        <extracomment>Settings page encrypted database</extracomment>
        <translation>Versleutelden databank</translation>
    </message>
    <message id="whisperfish-settings-encrypted-db-enabled">
        <location filename="../qml/pages/Settings.qml" line="346"/>
        <source>Enabled</source>
        <extracomment>Settings page encrypted db enabled</extracomment>
        <translation>Ingeschakeld</translation>
    </message>
    <message id="whisperfish-settings-encrypted-db-disabled">
        <location filename="../qml/pages/Settings.qml" line="349"/>
        <source>Disabled</source>
        <extracomment>Settings page encrypted db disabled</extracomment>
        <translation>Uitgeschakeld</translation>
    </message>
    <message id="whisperfish-verify-code-accept">
        <location filename="../qml/pages/Verify.qml" line="25"/>
        <source>Verify</source>
        <extracomment>Verify code accept</extracomment>
        <translation>Verifiëren</translation>
    </message>
    <message id="whisperfish-verify-code-title">
        <location filename="../qml/pages/Verify.qml" line="33"/>
        <source>Verify Device</source>
        <extracomment>Verify code page title</extracomment>
        <translation>Apparaatverificatie</translation>
    </message>
    <message id="whisperfish-verify-code-label">
        <location filename="../qml/pages/Verify.qml" line="43"/>
        <source>Code</source>
        <extracomment>Verify code label</extracomment>
        <translation>Code</translation>
    </message>
    <message id="whisperfish-verify-code-placeholder">
        <location filename="../qml/pages/Verify.qml" line="46"/>
        <source>123456</source>
        <extracomment>Verify code placeholder</extracomment>
        <translation>123456</translation>
    </message>
    <message id="whisperfish-voice-verify-code-instructions">
        <location filename="../qml/pages/Verify.qml" line="61"/>
        <source>Signal will call you with a 6-digit verification code. Please enter it here.</source>
        <extracomment>Voice verification code instructions</extracomment>
        <translation>Signal zal u bellen met ne verificatiecode van 6 tekens. Voerd deze hier in.</translation>
    </message>
    <message id="whisperfish-text-verify-code-instructions">
        <location filename="../qml/pages/Verify.qml" line="72"/>
        <source>Signal will text you a 6-digit verification code. Please enter it here, using only numbers.</source>
        <oldsource>Signal will text you a 6-digit verification code. Please enter it here.</oldsource>
        <extracomment>Text verification code instructions</extracomment>
        <translation>Signal stuurt u een SMS met een code van zes cijfers. Tikt die hier in.</translation>
    </message>
    <message id="whisperfish-verify-contact-identity-title">
        <location filename="../qml/pages/VerifyIdentity.qml" line="44"/>
        <source>Verify safety numbers</source>
        <oldsource>Verify %1</oldsource>
        <extracomment>Verify safety numbers</extracomment>
        <translation>Veiligheidsnummers verifiëren</translation>
    </message>
    <message id="whisperfish-group-add-member-menu">
        <location filename="../qml/pages/Group.qml" line="18"/>
        <source>Add Member</source>
        <extracomment>Add group member menu item</extracomment>
        <translation>Lid toevoegen</translation>
    </message>
    <message id="whisperfish-group-add-member-remorse">
        <location filename="../qml/pages/Group.qml" line="25"/>
        <source>Adding %1 to group</source>
        <oldsource>%1 added to group</oldsource>
        <extracomment>Add group member remorse message</extracomment>
        <translation>%1 word toegevoegd aan groep</translation>
    </message>
    <message id="whisperfish-group-leave-menu">
        <location filename="../qml/pages/Group.qml" line="35"/>
        <source>Leave</source>
        <extracomment>Leave group menu item</extracomment>
        <translation>Verlaten</translation>
    </message>
    <message id="whisperfish-group-leave-remorse">
        <location filename="../qml/pages/Group.qml" line="39"/>
        <source>Leaving group and removing ALL messages!</source>
        <oldsource>Leaving group. This will permanently remove ALL group messages!</oldsource>
        <extracomment>Leave group remorse message</extracomment>
        <translation>Groep word verlaten en ALLE berichten worden verwijderd!</translation>
    </message>
    <message id="whisperfish-group-members-title">
        <location filename="../qml/pages/Group.qml" line="62"/>
        <source>Group members</source>
        <extracomment>Group members</extracomment>
        <translation>Groepsleden</translation>
    </message>
</context>
</TS>
